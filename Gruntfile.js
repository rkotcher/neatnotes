var autoprefixer = require('autoprefixer');
var path = require('path');

var browserify = {
  'browser': {
    src: 'browser/router.jsx',
    dest: 'dist/browser/neatnotes.js',
    options: {
      transform: [['babelify', {
        presets: ['react', 'es2015'],
        plugins: ['syntax-object-rest-spread'],
      }]],
      browserifyOptions: {
        extensions: ['.js', '.jsx'],
        paths: [path.resolve()],
        debug: true,
      },
      keepAlive: false,
    },
  },
  'ext_background': {
    src: 'ext/background/index.js',
    dest: 'dist/ext/background.js',
    options: {
      transform: [['babelify', {
        presets: ['react', 'es2015'],
        plugins: ['syntax-object-rest-spread'],
      }]],
      browserifyOptions: {
        extensions: ['.js', '.jsx'],
        paths: [path.resolve()],
        debug: true,
      },
      keepAlive: false,
    },
  },
  'ext_contentScripts': {
    src: 'ext/content_scripts/index.js',
    dest: 'dist/ext/contentScripts.js',
    options: {
      transform: [['babelify', {
        presets: ['react', 'es2015'],
        plugins: ['syntax-object-rest-spread'],
      }]],
      browserifyOptions: {
        extensions: ['.js', '.jsx'],
        paths: [path.resolve()],
        debug: true,
      },
      keepAlive: false,
    },
  },
  'ext_browserAction': {
    src: 'ext/browser_action/action.js',
    dest: 'dist/ext/action.js',
    options: {
      transform: [['babelify', {
        presets: ['react', 'es2015'],
        plugins: ['syntax-object-rest-spread'],
      }]],
      browserifyOptions: {
        extensions: ['.js', '.jsx'],
        paths: [path.resolve()],
        debug: true,
      },
      keepAlive: false,
    },
  }
};

var concurrent = {
  'browser': {
    tasks: [
      'watch:browser_js',
      'watch:browser_less'
    ],
    options: { logConcurrentOutput: true },
  },
  'ext': {
    tasks: [
      'watch:ext_background_js',
      'watch:ext_contentScripts_js',
      'watch:ext_browserAction_js',
      'watch:ext_contentScripts_less',
      'watch:ext_browserAction_less',
      'watch:ext_browserAction_html',
      'watch:ext_manifest'
    ],
    options: { logConcurrentOutput: true },
  },
};

var exec = {
  'ext_copyIcons': 'cp ext/icons/* dist/ext',
  'ext_copyManifest': 'cp ext/manifest.json dist/ext/manifest.json',
  'ext_browserAction_copyHtml': 'cp ext/browser_action/index.html dist/ext/popup.html',
};

var less = {
  'browser': {
    options: {
      sourceMap: true,
      outputSourceFiles: true,
      paths: [path.resolve()],
    },
    files: {
      'dist/browser/neatnotes.css': 'browser/main.less',
    },
  },
  'ext_contentScripts': {
    options: {
      sourceMap: true,
      outputSourceFiles: true,
      paths: [path.resolve()],
    },
    files: {
      'dist/ext/contentScripts.css': 'ext/content_scripts/main.less',
    },
  },
  'ext_browserAction': {
    options: {
      sourceMap: true,
      outputSourceFiles: true,
      paths: [path.resolve()],
    },
    files: {
      'dist/ext/popup.css': 'ext/browser_action/popup.less',
    },
  },
};

var postcss = {
  'options': {
    map: true,
    processors: [ autoprefixer({ browsers: ['> 1%'] }) ],
  },
  'browser': {
    src: 'dist/browser/neatnotes.css',
    dest: 'dist/browser/neatnotes.postcss.css',
  },
  'ext_contentScripts': {
    src: 'dist/ext/contentScripts.css',
    dest: 'dist/ext/contentScripts.postcss.css',
  },
  'ext_browserAction': {
    src: 'dist/ext/popup.css',
    dest: 'dist/ext/popup.postcss.css',
  },
}

var watch = {
  'browser_js': {
    atBegin: true,
    files: ['browser/**/*.js', 'browser/**/*.jsx'],
    tasks: ['browserify:browser'],
  },
  'browser_less': {
    atBegin: true,
    files: ['browser/**/*.less'],
    tasks: ['less:browser', 'postcss:browser'],
  },
  'ext_background_js': {
    atBegin: true,
    files: ['ext/background/**/*.js', 'ext/background/**/*.jsx'],
    tasks: ['browserify:ext_background'],
  },
  'ext_contentScripts_js': {
    atBegin: true,
    files: ['ext/content_scripts/**/*.js', 'ext/content_scripts/**/*.jsx'],
    tasks: ['browserify:ext_contentScripts'],
  },
  'ext_browserAction_js': {
    atBegin: true,
    files: ['ext/browser_action/**/*.js', 'ext/browser_action/**/*.jsx'],
    tasks: ['browserify:ext_browserAction'],
  },
  'ext_contentScripts_less': {
    atBegin: true,
    files: ['ext/content_scripts/**/*.less'],
    tasks: ['less:ext_contentScripts', 'postcss:ext_contentScripts'],
  },
  'ext_browserAction_less': {
    atBegin: true,
    files: ['ext/browser_action/**/*.less'],
    tasks: ['less:ext_browserAction', 'postcss:ext_browserAction'],
  },
  'ext_browserAction_html': {
    atBegin: true,
    files: ['ext/browser_action/**/*.html'],
    tasks: ['exec:ext_browserAction_copyHtml'],
  },
  'ext_manifest': {
    atBegin: true,
    files: ['ext/manifest.json'],
    tasks: ['exec:ext_copyManifest'],
  }
};

module.exports = function(grunt) {
  grunt.initConfig({
    browserify: browserify,
    concurrent: concurrent,
    exec: exec,
    less: less,
    postcss: postcss,
    watch: watch,
  });

  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-postcss');

  grunt.registerTask('build:browser', [
    'less:browser',
    'postcss:browser',
    'browserify:browser'
  ]);

  grunt.registerTask('build:ext', [
    'exec:ext_copyIcons',
    'exec:ext_copyManifest',
    'less:ext_contentScripts',
    'less:ext_browserAction',
    'postcss:ext_contentScripts',
    'browserify:ext_background',
    'browserify:ext_contentScripts',
    'browserify:ext_browserAction'
  ]);

  grunt.registerTask('monitor:browser', [
    'concurrent:browser'
  ]);

  grunt.registerTask('monitor:ext', [
    'concurrent:ext'
  ]);
};
