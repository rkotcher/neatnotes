import $ from 'jquery';
import config from '../../config';

let _LAST_CONTEXT_MENU_POSITION_X = 0;
let _LAST_CONTEXT_MENU_POSITION_Y = 0;

window.oncontextmenu = function(e) {
  _LAST_CONTEXT_MENU_POSITION_X = e.pageX - window.scrollX;
  _LAST_CONTEXT_MENU_POSITION_Y = e.pageY - window.scrollY;
};

$('body').addClass(config.pluginIdentifier);

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {

  const parameters = {
    'x': _LAST_CONTEXT_MENU_POSITION_X,
    'y': _LAST_CONTEXT_MENU_POSITION_Y,
  };

  sendResponse({
    'viewportWidth': window.innerWidth,
    'viewportHeight': window.innerHeight,
    'mouseX': parameters.x,
    'mouseY': parameters.y,
  });
});
