import _ from 'underscore';
import $ from 'jquery';

import config from '../../config';
import socketIo from 'socket.io-client';

function captureDataAndCreateNote(info, tab) {
  chrome.tabs.sendMessage(tab.id, {}, function(response) {
    chrome.tabs.captureVisibleTab(null, { 'format': 'png' }, base64Image => {
      $.ajax({
        'url': `${ config.location }/api/note`,
        'type': 'POST',
        'contentType': 'application/json',
      }).then(emptyObjectWithId => {

        // NOTE 1) Open a window and start polling for the new note
        chrome.windows.create({ 'url': `${ config.location }/note/${ emptyObjectWithId._id }` });

        // NOTE 2) Startup uploading the data
        $.ajax({
          'url': `${ config.location }/api/note/${ emptyObjectWithId._id }`,
          'type': 'POST',
          'contentType': 'application/json',
          'data': JSON.stringify({
            'href': tab.url,
            'screenshot': {
              'base64': base64Image,
              'viewportWidth': response.viewportWidth,
              'viewportHeight': response.viewportHeight,
            },
            'clickLocation': {
              'mouseX': response.mouseX,
              'mouseY': response.mouseY,
            },
          }),
        });
      }).fail(err => {
        console.error(err);
      });
    });
  });
}

chrome.contextMenus.create({
  'title': 'Create NeatNote...',
  'contexts': ['page', 'selection', 'image', 'link'],
  'onclick': captureDataAndCreateNote,
});

chrome.notifications.onClicked.addListener(url => {
  chrome.tabs.create({ 'url': url });
});

setInterval(() => {
  $.get(`${ config.location }/api/notification/INITIAL`).then(notifications => {
    notifications.forEach(notification => {
      chrome.notifications.create(`${ config.location }/note/${ notification.data.note._id }`, {
        'type': 'basic',
        'iconUrl': 'neatnotes.png',
        'title': `${ notification.data.from.displayName } mentioned you in a NeatNote!`,
        'message': 'Click here to see what they said.'
      }, () => {
        $.ajax({
          'type': 'POST',
          'url': `${ config.location }/api/notification/${ notification._id }/NOTIFIED`,
          'contentType': 'application/json',
        });
      });
    });
  }).fail(_.noop);;
}, 10000);
