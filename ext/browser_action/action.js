import $ from 'jquery';
import ReactDOM from 'react-dom';
import React from 'react';
import config from '../../config';

import { MuiThemeProvider } from 'material-ui/styles';
import {List, ListItem} from 'material-ui/List';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import {pinkA200, transparent} from 'material-ui/styles/colors';

function visitNote(noteId, notificationId) {
  window.open(`${ config.location }/note/${ noteId }`);
  $.ajax({
    'type': 'POST',
    'url': `${ config.location }/api/notification/${ notificationId }/READ`,
    'contentType': 'application/json',
  });
}

function visitNeatnotes() {
  window.open(config.location);
}

function loginWithFacebook() {
  FB.login(function(response) {
    console.log(response);
    if (response.authResponse) {
      renderList();
    }
  });
}

function _renderItem(item, index, isRead) {
  const classes = isRead ? 'dropdown-item' : 'dropdown-item read';
  return (
    <div
      className={ classes }
      key={ index }
      onClick={ () => visitNote(item.data.note._id, item._id) }
    >
      <img src={ `http://graph.facebook.com/${ item.data.note.author.facebookId }/picture` } />
      <div className='note-info'>
        <div className='note-href'>
        { item.data.note.href }
        </div>
        <div className='note-from'>
        from { item.data.from.displayName }
        </div>
      </div>
    </div>
  );
}

function renderList() {
  $.get(`${ config.location }/api/notification/NOTIFIED`, notified => {
    $.get(`${ config.location }/api/notification/READ`, read => {
      const list = (
        <div>
          <p className='link-to-neatnotes border-bottom'>
            Visit <a href='#' onClick={ visitNeatnotes }>{ config.location }</a> to browse tags.
          </p>
          {
            notified.length == 0 && read.length == 0 ?
              <p className='empty-message'>
                Nobody has mentioned you in a note...yet! Try posting one.
              </p>
            :
              <div className='dropdown-content'>
                { notified.map((item, index) => _renderItem(item, index, false)) }
                { read.map((item, index) => _renderItem(item, index, true)) }
              </div>
          }
        </div>
      );

      ReactDOM.render(list, document.getElementById('popup-app'));
    })
  });
}

function renderLogin() {
  const loginWidget = (
    <div className='link-to-neatnotes'>
      <p>
        Please <a href='#' onClick={ visitNeatnotes }>sign in</a> to be notified when somebody mentions you in a note.
      </p>
    </div>
  );

  ReactDOM.render(loginWidget, document.getElementById('popup-app'));
}

window.onload = () => {
  $.get(`${ config.location }/api/auth/session`).then(session => {
    if (session._id) {
      renderList();
    } else {
      renderLogin();
    }
  });
};
