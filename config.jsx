import _ from 'underscore';
import env from './environment';

if (!env) {
  throw 'Fatal: You didn\'t set NEATNOTES_ENV!'
}

if (env != 'development' && env != 'production') {
  throw 'Fatal: NEATNOTES_ENV must be "development" or "production"';
}

let defaults = {
  'pluginIdentifier': '__neatnotes_identifier_do_not_remove_or_you_will_be_fired__',
  'description': 'Configure the neatest notes in the world.',
};

if (env == 'development') {
  defaults = _.extend(defaults, {
    'db': {
      'name': 'neatnotes',
      'path': 'mongodb://localhost:27017/neatnotes',
      'port': 27017,
      'host': 'localhost',
    },
    'host': 'http://localhost',
    'port': 3000,
    'socketClient': 'http://localhost',
    'location': 'http://localhost:3000',
    'facebookAppId': '133884750385781',
  });
}

if (env == 'production') {
  defaults = _.extend(defaults, {
    'db': {
      'name': 'neatnotes',
      'path': 'mongodb://localhost:27017/neatnotes',
      'port': 27017,
      'host': 'localhost',
    },
    'host': 'http://www.neatnotes.io',
    'port': 80,
    'socketClient': 'http://www.neatnotes.io',
    'location': 'http://www.neatnotes.io',
    'facebookAppId': '133837567057166',
  });
}

export default defaults;
