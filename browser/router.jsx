import $ from 'jquery';
import Backbone from 'backbone';
import ReactDOM from 'react-dom';
import React from 'react';

import config from '../config';

import session from './apps/session';
import Note from './apps/note/Note';
import Splash from './apps/splash/Splash';

class Router extends Backbone.Router {
  routes() {
    return {
      'note/:id': 'note',
      '(/)': 'splash',
    };
  }

  note(id) {
    this.render(<Note id={ id } />)
  }

  splash() {
    $.get('/api/auth/session').then(user => {
      user._id && this.render(<Note />);
      !user._id && this.render(<Splash />);
    });
  }

  render(view) {
    ReactDOM.render(view, document.getElementById('neatnotes'));
  }
}
const router = new Router();

window.fbAsyncInit = function() {
  FB.init({
    'appId': config.facebookAppId,
    'xfbml': true,
    'version': 'v2.7'
  });

  FB.getLoginStatus();

//FB.api("/112662965852123/friends",function (response) {console.log(response);});
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = '//connect.facebook.net/en_US/sdk.js';
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

$(document).ready(() => {
  Backbone.history.start({ pushState: true });
});
