import _ from 'underscore';
import React from 'react';

export default React.createClass({
  getInitialState() {
    return {
      'dropdownOpened': false,
      'notes': [],
      'searchValue': '',
    };
  },

  componentDidMount() {
    $.get('/api/note').then(notes => {
      this.setState({ 'notes': notes });
    });
  },

  onDropdownBlur() {
    setTimeout(() => {
      if (!$(this.refs.dropdownContent).is(':focus')) {
        this.setState({ 'dropdownOpened': false });
      }
    });
  },

  logout() {
    $.ajax({
      'type': 'POST',
      'url': '/api/auth/logout',
      'success': () => window.location = '/',
      'error': () => console.error('Could not log out.')
    });
  },

  render() {
    return (
      <div id='main-navbar'>
        <div className='dropdown-container'>
          <input
            onChange={ () => { this.setState({ 'searchValue': $(this.refs.searchInput).val() }) } }
            onBlur={ this.onDropdownBlur }
            onFocus={ () => { this.setState({ 'dropdownOpened': true }) } }
            ref='searchInput'
            className='searchbox'
            placeholder='Search by your hashtags within notes'
            value={ this.state.searchValue }
          />
          {
            this.state.dropdownOpened && this.state.notes.length > 0 ?
              <div className='dropdown-content' ref='dropdownContent' tabIndex='-1'>
              {
                this.state.notes.map((note, index) => {
                  if (this.state.searchValue && _.every(note.tags, tag => {
                    return tag.tagText.indexOf(this.state.searchValue) == -1;
                  })) {
                    return <span/>;
                  }

                  return (
                    <div
                      className='dropdown-item'
                      key={ index }
                      onClick={ () => window.location = `/note/${ note._id }` }
                    >
                      <img src={ `http://graph.facebook.com/${ note.author.facebookId }/picture` } />
                      <div className='note-info'>
                        <div className='note-href'>
                        { note.href }
                        </div>
                        <div className='note-tags'>
                        {
                          note.tags ?
                            <div>
                            {
                              note.tags.map((tag, index) => {
                                return <span key={ index } className='text-tag'>#{ tag.tagText }</span>;
                              })
                            }
                            </div>
                          :
                            <div>This note hasn't been tagged yet</div>
                        }
                        </div>
                      </div>
                    </div>
                  );
                })
              }
              </div>
            :
              null
          }
        </div>
        <div>
          <a href='http://neatnotes.freeforums.net/'>Discussion</a>
          <a href='https://docs.google.com/document/d/1_nyVocH0j1pspSFjzju3AgLv9_4757lAwrci7Vk3y9Y/edit?usp=sharing' target='_blank'>Guide</a>
          <a href='' onClick={ this.logout }>Logout</a>
        </div>
      </div>
    );
  },
});
