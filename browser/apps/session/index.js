function get() {
  return $.get('/api/auth/session');
}

export default {
  'get': get,
};
