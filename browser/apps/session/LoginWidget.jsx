import React from 'react';

import config from '../../../config';

export default React.createClass({
  facebookLogin() {
    FB.login(login => {
      FB.api('/me?fields=email,name', me => {
        if (login.authResponse.accessToken) {
          $.ajax({
            url: '/api/auth/facebook',
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
              'accessToken': login.authResponse.accessToken,
              'displayName': me.name,
              'email': me.email,
            }),
            success: this.props.onLogin,
            error: this.props.onError,
          });
        }
      });
    }, { scope: 'public_profile,email,user_friends' });
  },

  render() {
    return (
      <div className='login-widget'>
        <button onClick={ this.facebookLogin } className='button-facebook'>
          Login with Facebook
        </button>
      </div>
    );
  },
});
