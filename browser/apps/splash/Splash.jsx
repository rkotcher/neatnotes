import React from 'react';
import LoginWidget from '../session/LoginWidget';
import FakeChatWindow from './FakeChatWindow';

export default React.createClass({
  componentDidMount() {
    $('#typing-content').typed({
      'backDelay': 50,
      'strings': ['Organize the', 'Discuss and organize the Internet.'],
      'typeSpeed': 25,
      'showCursor': false,
    });
  },

  render() {
    return (
      <div id='splash'>
        <img src='https://www.theansr.com/assets/beta-ribbon-c364f46251b126aef474ef6f9e6693b1.png' style={{ 'position': 'absolute', 'top': 0, 'left': 0 }}></img>
        <div className='splash-navbar'>
          <a href='http://neatnotes.freeforums.net/' target='_blank'>Discussion</a>
          <a href='https://docs.google.com/document/d/1_nyVocH0j1pspSFjzju3AgLv9_4757lAwrci7Vk3y9Y/edit?usp=sharing' target='_blank'>Guide</a>
        </div>
        <div className='typing-banner'>
          <h1>Bookmarks for the 22nd Century.</h1>
          <div id='typing-content' className='typing-content'/>
          <LoginWidget onLogin={ () => window.location.reload() } />
        </div>
        <div className='fake-chat-container'>
          <FakeChatWindow />
        </div>
        <div className='splash-footer'>
          Made with love by <a href='mailto:rkotcher@gmail.com'>wheresmycookie</a>
        </div>
      </div>
    );
  },
});
