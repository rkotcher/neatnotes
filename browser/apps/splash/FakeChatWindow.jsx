import assert from 'assert';
import React from 'react';

import config from '../../../config';
import ChatMessage from '../note/chat_window/ChatMessage';
import TextInput from '../note/chat_window/TextInput';
import session from '../session'

function Message(params) {
  assert.notEqual(params.user._id,  undefined);
  assert.notEqual(params.user.facebookId, undefined);
  assert.notEqual(params.text, undefined);
  return { 'text': params.text, 'user': params.user };
}

export default React.createClass({
  render() {
    const messages = [
      {"text":"Hello!","_id":"57bafba651180c8446d84e20","user":{"_id":"57baf6626aa92255452af4e3","facebookId":10206524322014828,"displayName":"Robert Kotcher","email":"rkotcher@gmail.com","__v":0}},
      {"text":"I found something that I think you'll love.","_id":"57bafba651180c8446d84e20","user":{"_id":"57baf6626aa92255452af4e3","facebookId":10206524322014828,"displayName":"Robert Kotcher","email":"rkotcher@gmail.com","__v":0}},
       ];

    return (
      <span>
        <div className='blinking-pointer fake' />
        <div id='fake-chat-window' className='chat-window animated bounceIn fake'>
          <div className='navbar'/>
          <div className='chat-content-container' ref='contentScrollContainer'>
          {
            messages.map((message, index) => {
              return (
                <ChatMessage
                  key={ index }
                  text={ message.text }
                  user={ message.user }
                />
              );
            })
          }
          </div>
          <TextInput/>
        </div>
      </span>
    );
  },
});
