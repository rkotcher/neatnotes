export default {
  componentDidMount() {
    $(window).resize(() => { this.forceUpdate(); });
  },

  componentWillUnmount() {
    $(window).off('resize');
  },
};
