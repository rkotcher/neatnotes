import assert from 'assert';
import async from 'async';
import React from 'react';

import config from '../../../config';
import AccountPrompt from './AccountPrompt';
import ChatWindow from './chat_window/ChatWindow';
import MainNavbar from '../MainNavbar';
import ResizeResponderMixin from './ResizeResponderMixin';

export default React.createClass({
  getInitialState() {
    return {
      'showAccountPrompt': false,
      'note': null,
      'user': null,
    };
  },

  mixins: [ResizeResponderMixin],

  onCompleteSetup() {
    window.location.reload();
  },

  componentDidMount() {
    $('body').addClass('giftly');

    if (!this.props.id) return;

    // At this point, we know we're trying to get a particular note, but
    // there's a chance that it actually doesn't exist yet. Continue retrying
    // for awhile, and show a loading icon.
    async.retry(
      { 'times': 30, 'interval': 2000 },
      (callback, results) => {
        $.get(`/api/note/${ this.props.id }`).then(note => {
          this.setState({ 'note': note });
          callback(null, note);
        }).fail(callback);
      }, this.checkSignupComplete
    );
  },

  checkSignupComplete() {
    $.get('/api/auth/session', user => {
      setTimeout(() => {
        this.setState({
          'showAccountPrompt': !user._id || !$('body').hasClass(config.pluginIdentifier),
          'user': user,
        });
      }, 1000);
    });
  },

  componentDidUpdate() {
    const animation = {};

    if (parseInt($(this.refs.hiddenContainer).css('width')) > window.innerWidth) {
      animation['scrollLeft'] = this.state.note.clickLocation.mouseX - ((window.innerWidth - 320) / 2);
    } else {
      animation['scrollLeft'] = 0;
    }

    if (parseInt($(this.refs.noteContainer).css('margin-bottom'))) {
//      animation['scrollTop'] = this.state.note.clickLocation.mouseY - 180;
      animation['scrollTop'] = this.state.note.clickLocation.mouseY - ((window.innerHeight - 400) / 2);

      const SCROLL_Y_THRESHOLD = 32;
      if (this.state.note.clickLocation.mouseY < SCROLL_Y_THRESHOLD + $('body').scrollTop() + 60) {
        animation['scrollTop'] = this.state.note.clickLocation.mouseY - 60 - SCROLL_Y_THRESHOLD;
      }
    } else {
      animation['scrollTop'] = 0;
    }

    setTimeout(() => {
      $('body').stop().animate(animation, 250, 'swing');
    }, 50);
  },

  getContainerStyles(chatWindow) {
    const chatBounds = {
      'x': (window.innerWidth / 2) - (chatWindow.props.width / 2),
      'y': (window.innerHeight / 2) - (chatWindow.props.height / 2),
      'width': chatWindow.props.width,
      'height': chatWindow.props.height,
    };

    return {
      'noteContainer': {
        'position': 'absolute',
        'width': this.state.note.screenshot.viewportWidth,
        'height': this.state.note.screenshot.viewportHeight,
        'marginLeft': Math.max(chatBounds.x - this.state.note.clickLocation.mouseX, 0),
        'marginTop': Math.max(chatBounds.y - this.state.note.clickLocation.mouseY, 0),
        'marginBottom': this.state.note.screenshot.viewportHeight,
      },
      'hiddenContainer': {
        'height': 1,
        'width': window.innerWidth + Math.max(this.state.note.clickLocation.mouseX - chatBounds.x, 0),
      },
    };
  },

  render() {
    if (this.props.id && !this.state.note) {
      return (
        <div id='note'>
          <div className='loading-gif'/>
        </div>
      );
    }

    if (!this.props.id) {
      return (
        <div id='note'>
          <MainNavbar />
          <div className='empty-message'>
            <h1>Howdy!</h1>
            <p>If you need the plugin, you can get it <a target='_blank' href='https://chrome.google.com/webstore/detail/neatnotes/ghjckgohacgaekkinpmhelbekakhpfhm'>here</a>.</p>
            <p>Otherwise, view your notes in the navbar above, or browse the Internet for a cat picture and start a discussion. You can read the quick-start guide <a target='_blank' href='https://docs.google.com/document/d/1_nyVocH0j1pspSFjzju3AgLv9_4757lAwrci7Vk3y9Y/edit'>here</a>.</p>
          </div>
        </div>
      );
    };

    const clickPosition = {
      'left': `${ this.state.note.clickLocation.mouseX }px`,
      'top': `${ this.state.note.clickLocation.mouseY}px`,
    };

    const chatWindow = (
      <ChatWindow { ...clickPosition } user={ this.state.user } note={ this.state.note }/>
    );
    const screenshotUrl = `/api/file/${ this.state.note.screenshot.fileId }`;
    const styles = this.getContainerStyles(chatWindow);

    // XXX This is a terrible way to do this
    if (this.state.user && this.state.note) {
      $.ajax({
        url: `/api/note/${ this.state.note._id }/add_user`,
        type: 'POST',
      });
    }

    return (
      <div id='note'>
        <MainNavbar user={ this.state.user }/>
        <div style={ styles.noteContainer } ref='noteContainer'>
          <img className='note-screenshot' src={ screenshotUrl }/>
          <div style={ styles.hiddenContainer } ref='hiddenContainer'/>
          {
            this.state.showAccountPrompt ?
              <AccountPrompt onCompleteSetup={ this.onCompleteSetup }/>
            :
              <span/>
          }
          { chatWindow }
        </div>
      </div>
    );
  },
});
