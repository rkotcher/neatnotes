import React from 'react';

import { MuiThemeProvider } from 'material-ui/styles';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Step,
  StepContent,
  StepLabel,
  Stepper,
} from 'material-ui/Stepper';

import config from '../../../config';
import LoginWidget from '../session/LoginWidget';

export default React.createClass({
  getInitialState() {
    return {
      'currentStep': 1,
      'displayName': '',
      'pluginInstalled': false,
      'wentToDownload': false,
    };
  },

  componentDidMount() {
    $('#shadow-container').stop().animate({ 'opacity': 0.5 }, 1000);

    setTimeout(() => {
      $.get('/api/auth/session').then(response => {
        response._id && this.setState({
          'currentStep': 2,
          'displayName': response.displayName || '',
        });
      });
    }, 300);

    this.pluginChecker = window.setInterval(() => {
      if ($('body').hasClass(config.pluginIdentifier) && this.state.currentStep == 2) {
        this.props.onCompleteSetup();
      }
    }, 1000);
  },

  componentWillUnmount() {
    window.clearInterval(this.pluginChecker);
  },

  onLogin(response) {
    response.displayName && this.setState({ 'displayName': response.displayName });
    this.setState({ 'currentStep': 2 });
  },

  onGoToDownload() {
    window.open('https://chrome.google.com/webstore/detail/neatnotes/ghjckgohacgaekkinpmhelbekakhpfhm', '');
    this.setState({ 'wentToDownload': true });
  },

  render() {
    const connectText = this.state.displayName ? `Welcome, ${ this.state.displayName }!` : 'Connect.';
    return (
      <div id='account-prompt'>
        <div id='shadow-container'/>
        <MuiThemeProvider>
          <div className='stepper-container'>
            <h1>Join the discussion!</h1>
            <h2>Getting set up is easy and secure.</h2>
            <Stepper activeStep={ this.state.currentStep } orientation='vertical'>
              <Step>
                <StepLabel>Receive an invite.</StepLabel>
                <StepContent/>
              </Step>
              <Step>
                <StepLabel>{ connectText }</StepLabel>
                <StepContent>
                  <p className='step-content-text'>It's nice to meet you.</p>
                  <LoginWidget onLogin={ this.onLogin }/>
                  <p className='step-content-text italic'>
                    We use this information so others know who you are when you share a note.
                  </p>
                </StepContent>
              </Step>
              <Step>
                <StepLabel>Download the plugin.</StepLabel>
                <StepContent>
                {
                  !this.state.wentToDownload ?
                    <span>
                      <p className='step-content-text'>The plugin lets you create notes, and lets you know when others have sent you a note.</p>
                      <p className='step-content-text-small'>NeatNotes currently works in Google Chrome, but we'll offer support for other platforms soon.</p>
                      <RaisedButton label='Download' primary={ true } onClick={ this.onGoToDownload }/>
                      <p className='step-content-text italic'>
                        You can change your notification settings after logging into your account.
                      </p>
                    </span>
                  :
                    <p>Click <a onClick={ () => window.location.reload() } href='#'>refresh</a> after downloading the plugin.</p>
                }
                </StepContent>
              </Step>
            </Stepper>
          </div>
        </MuiThemeProvider>
      </div>
    );
  },
});
