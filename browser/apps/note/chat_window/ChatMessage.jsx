import classnames from 'classnames';
import React from 'react';

export default React.createClass({
  getDefaultProps() {
    return {
      'isMine': true,
    };
  },

  render() {
    return (
      <div className='chat-message is-mine animated fadeIn'>
        <img className='chat-icon' src={ `http://graph.facebook.com/${ this.props.user.facebookId }/picture` } />
        <div className='chat-textbubble-and-name-container'>
          <span className='chat-textbubble'><span className='username'>Robert:</span> { this.props.text }</span>
          <div className='chat-textbubble-span'/>
        </div>
      </div>
    );
  },
});
