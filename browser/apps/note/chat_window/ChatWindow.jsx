import assert from 'assert';
import React from 'react';

import config from '../../../../config';
import ChatMessage from './ChatMessage';
import TextInput from './TextInput';
import socketIo from 'socket.io-client';
import session from '../../session'

function Message(params) {
  assert.notEqual(params.user._id,  undefined);
  assert.notEqual(params.user.facebookId, undefined);
  assert.notEqual(params.text, undefined);
  return { 'text': params.text, 'user': params.user };
}

export default React.createClass({
  getDefaultProps() {
    return {
      'width': 320,
      'height': 400,
    };
  },

  getInitialState() {
    return {
      'loaded': false,
      'showing': true,
      'note': null,
    };
  },

  componentDidMount() {
    this.setState({ 'note' : this.props.note });
    setTimeout(() => this.setState({ 'loaded': true }), this.props.delay);

    this.socket = socketIo();
    this.socket.emit('register_message', { 'noteId': this.props.note._id });
    this.socket.on('message', message => {
      this.receiveMessage(message);
    });

    window.onbeforeunload = () => {
      this.socket.emit('unregister_message', { 'noteId': this.props.note._id });
    };

    setTimeout(this.scrollToBottom);
  },

  componentWillUnmount() {
    this.socket.emit('unregister_message', { 'noteId': this.props.note._id });
    window.onbeforeunload = null;
  },

  scrollToBottom() {
    const scrollContainer = this.refs.contentScrollContainer;
    scrollContainer.scrollTop = scrollContainer.scrollHeight;
  },

  sendMessage(text) {
    $.get('/api/auth/session').then(session => {
      const note = this.state.note;
      const message = Message({ 'text': text, 'user': session });

      return $.ajax({
        'url': '/api/message',
        'type': 'POST',
        'contentType': 'application/json',
        'data': JSON.stringify({
          'noteId': note._id,
          'message': message,
        }),
      });
    });
  },

  receiveMessage(message) {
    const note = this.state.note;
    note.messages.push(message);
    this.setState({ 'note': note }, this.scrollToBottom);
  },

  toggleNote() {
    this.setState({ 'showing': !this.state.showing });
  },

  render() {
    if (!this.state.loaded) return <span />;

    const windowStyles = {
      'width': this.props.width,
      'height': this.props.height,
      'top': this.props.top,
      'left': this.props.left,
      'display': this.state.showing ? 'flex' : 'none',
    };

    const blinkerStyles = {
      'top': this.props.top,
      'left': this.props.left,
    };

    const messages = this.state.note ? this.state.note.messages : [];

    return (
      <span>
        <div style={ blinkerStyles } className='blinking-pointer' onClick={ this.toggleNote }/>
        <div id='chat-window' className='chat-window animated bounceIn' ref='chatWindow' style={ windowStyles }>
          <div className='navbar'>
            <i className='navbar-close' onClick={ this.toggleNote }/>
            <a className='navbar-open' target='_blank' href={ this.state.note.href }>visit page</a>
          </div>
          <div className='chat-content-container' ref='contentScrollContainer'>
          {
            messages.map((message, index) => {
              return (
                <ChatMessage
                  key={ index }
                  text={ message.text }
                  user={ message.user }
                />
              );
            })
          }
          </div>
          <TextInput onNewMessage={ this.sendMessage } note={ this.props.note }/>
        </div>
      </span>
    );
  },
});
