const React = require('react');
const ReactDOM = require('react-dom');

const TextInput = React.createClass({
  getInitialState() {
    return {
      'atJsShowing': false,
      'mentions': [],
      'value': '',
    };
  },

  componentDidMount() {
    this.setOverlay();
    $(this.refs.textarea).focus();
    $.get('/api/auth/session').then(session => {
      FB.api(`/${ session.facebookId }/friends`, response => {
        FB.getLoginStatus(() => {
          $(this.refs.textarea).atwho({
            at: '@',
            data: response.data,
            displayTpl: '<li data-facebook-id="${id}">${name}</li>',
          });
          $(this.refs.textarea).on('inserted.atwho', (e, item, browserEvent) => {
            // FIXME FIXME FIXME not good
            const dummy = document.createElement('div');
            dummy.innerHTML = item[0].outerHTML;
            const mention = {
              'facebookId': $(dummy.childNodes[0]).data('facebook-id'),
              'name': item[0].innerHTML,
            };

            const currentMentions = this.state.mentions;
            currentMentions.push(mention);
            browserEvent.stopPropagation();
          });
          $(this.refs.textarea).on('shown.atwho', () => {
            this.setState({ 'atJsShowing': true });
          });
          $(this.refs.textarea).on('hidden.atwho', () => {
            this.setState({ 'atJsShowing': false });
          });
        });
      });
    });
  },

  setOverlay() {
    $(this.refs.textarea).overlay([
      {
        match: /\B#\w+/g,
        css: {
          'background-color': '#d8dfea',
          'border': '1px solid #5977a6',
          'border-radius': '3px'
        }
      }
    ]);
  },

  onKeyDown(e) {
    if (e.keyCode == 13 && !this.state.atJsShowing) {
      this.state.mentions.forEach(mention => {
        if (e.target.value.indexOf(mention.name > 0)) {
          $.ajax({
            'type': 'POST',
            'url': '/api/notification',
            'contentType': 'application/json',
            'data': JSON.stringify({
              'recipientFacebookId': mention.facebookId,
              'noteId': this.props.note._id,
            }),
          });
        }
      });

      this.props.onNewMessage(e.target.value);
      $(this.refs.textarea).val('');
      this.setOverlay();
      this.setState({ 'mentions': [] });
      e.preventDefault();
    }
    this.adjustHeight();
  },

  adjustHeight() {
    const textArea = ReactDOM.findDOMNode(this.refs.textarea);
    $(textArea).css('height', 'auto');
    $(textArea).height(textArea.scrollHeight - 24);
  },

  render() {
    return (
      <div className='text-input-container'>
        <textarea
          rows='1'
          ref='textarea'
          disabled={ window.location.pathname == '/' }
          onKeyDown={ this.state.atJsShowing ? () => {} : this.onKeyDown }
          placeholder='Press Enter to send'
        />
      </div>
    );
  }
});

module.exports = TextInput;
