import assert from 'assert';
import bodyParser from 'body-parser';
import ConnectMongo from 'connect-mongo';
import cookieParser from 'cookie-parser';
import emojic from 'emojic';
import express from 'express';
import Grid from 'gridfs-stream';
import mongo from 'mongodb';
import path from 'path';
import session from 'express-session';
import socketIo from 'socket.io';

import config from '../config';
import routes from './routes/routes';

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

http.listen(config.port, function () {
  console.log(`${ emojic.thumbsUp }: server is running on port ${ config.port }.`);
});

const MongoStore = ConnectMongo(session);
const db = new mongo.Db(config.db.name, new mongo.Server(config.db.host,
    config.db.port));

app.use(bodyParser.json({ 'limit': '25mb' }))
app.use(cookieParser());
app.use(express.static('dist/browser'));
app.use(session({
  secret: 'Keyb0ard Kat ^-^',
  resave: false,
  saveUninitialized: true,
  store: new MongoStore({ url: config.db.path })
}));

routes(app, io);

app.get('/*', function(req, res, next) {
  res.sendFile('index.html', {
    root: 'dist/browser',
  });
});

db.open(err => {
  if (err) {
    return console.log(`${ emojic.thumbsDown }: Can't init db for Gridfs.`);
  }
  console.log(`${ emojic.thumbsUp }: Init'd db for Gridfs.`);
  app.set('gridfs', Grid(db, mongo));
});
