import _ from 'underscore';
import assert from 'assert';
import emojic from 'emojic';
import mongoose from 'mongoose';

import config from '../config';

const Schema = mongoose.Schema;

mongoose.connect(config.db.path, err => {
  err && console.log(`${ emojic.thumbsDown }: mongoose is not talking to mongodb.`);
  !err && console.log(`${ emojic.thumbsUp }: mongoose is talking to mongodb.`);
});

const MessageSchema = new Schema({
  'userId': String,
  'text': String,
});

const Note = mongoose.model('Note', {
  'authorId': Schema.Types.ObjectId,
  'href': String,
  'screenshot': {
    'fileId': Schema.Types.ObjectId,
    'viewportWidth': Number,
    'viewportHeight': Number,
  },
  'clickLocation': {
    'mouseX': Number,
    'mouseY': Number,
  },
  'messages': [MessageSchema],
});

const NoteUserAssociation = mongoose.model('NoteUserAssociation', {
  'noteId': Schema.Types.ObjectId,
  'userId': Schema.Types.ObjectId,
});

const Notification = mongoose.model('Notification', {
  'type': {
    'type': String,
    'enum': ['MENTION'],
    'default': 'MENTION',
  },
  'status': {
    'type': String,
    'enum': ['INITIAL', 'NOTIFIED', 'READ'],
    'default': 'INITIAL',
  },
  'recipientId': Schema.Types.ObjectId,
  'data': Schema.Types.Mixed,
});

const NoteUserTagAssociation = mongoose.model('NoteUserTagAssociation', {
  'noteId': Schema.Types.ObjectId,
  'userId': Schema.Types.ObjectId,
  'tagText': String,
});

const Message = {
  'create': params => {
    assert.notEqual(params.noteId, undefined);
    assert.notEqual(params.userId, undefined);
    assert.notEqual(params.text, undefined);

    return new Promise((resolve, reject) => {
      const query = {
        '_id': params.noteId,
      };
      Note.findOne({ '_id': params.noteId }).then(note => {
        if (!note._id) return reject();

        note.messages.push({
          'userId': params.userId,
          'text': params.text,
        });

        note.save(err => {
          err && reject(err);
          !err && resolve(_.last(note.messages));
        });
      });
    });
  },
};

const User = mongoose.model('User', {
  'displayName': String,
  'email': String,
  'facebookId': Number,
});

export {
  Message,
  Note,
  NoteUserAssociation,
  NoteUserTagAssociation,
  Notification,
  User
};
