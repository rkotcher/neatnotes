import express from 'express';

import apiRoutes from './api';

export default function(app, io) {
  app.use('/api', apiRoutes(app, io));
}
