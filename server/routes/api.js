import express from 'express';

import authRoutes from './api/auth';
import fileRoutes from './api/file';
import noteRoutes from './api/note';
import messageRoutes from './api/message';
import notificationRoutes from './api/notification';

const router = express.Router();

export default function(app, io) {
  router.use('/auth', authRoutes(app));
  router.use('/file', fileRoutes(app));
  router.use('/note', noteRoutes(app));
  router.use('/message', messageRoutes(app, io));
  router.use('/notification', notificationRoutes(app, io));

  router.use((err, req, res, next) => {
    console.error('API ERROR', err, req.body, req.params, err.stack);
    res.status(500);
    res.json({
      'message': err.message,
      'name': err.name,
      'stack': err.stack,
    });
  });

  return router;
}
