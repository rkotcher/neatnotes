import assert from 'assert';
import express from 'express';

import { Note, Notification, User } from '../../models';
import { fillNote } from './note';

const router = express.Router();

// notification happens when somebody is "mentioned" in a note

function fillNotification(notification) {
  notification = notification.toObject();

  return new Promise((resolve, reject) => {
    const recipientParams = { '_id': notification.recipientId };
    const fromParams = { '_id': notification.data.fromId };

    User.findOne(recipientParams, (err, recipient) => {
        if (err) return reject(err);

      User.findOne(fromParams, (err, from) => {
        if (err) return reject(err);

        notification.recipient = recipient;
        notification.data.from = from;
        delete notification.recipientId;
        delete notification.data.fromId;

        const noteParams = {
          '_id': notification.data.noteId,
        };
        Note.findOne(noteParams, (err, note) => {
          if (err) return reject(err);
          fillNote(note).then(note => {
            delete note.messages;
            notification.data.note = note;
            delete notification.data.noteId;
            resolve(notification);
          }).catch(reject);
        });
      });
    });
  });
}

export default function(app) {
  router.get('/:status', (req, res, next) => {
    if (!req.session.userId) {
      return res.json({ 'error': 'You must be logged in to get your notifications.' });
    }
    const params = {
      'recipientId': req.session.userId,
      'status': req.params.status,
    };
    Notification.find(params, (err, notifications) => {
      if (err) return next(err);
      Promise
        .all(notifications.map(notification => fillNotification(notification) ))
        .then(filledNotifications => res.json(filledNotifications), next);
    });
  });

  router.post('/', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must be logged in to create this notification.');
    }
    User.findOne({ 'facebookId': req.body.recipientFacebookId }, (err, user) => {
      if (err) return next(err);

      Notification.create({
        'recipientId': user._id,
        'type': 'MENTION', // XXX: only one type of notification right now.
        'data': {
          'fromId': req.session.userId,
          'noteId': req.body.noteId,
        },
      }, (err, notification) => {
        if (err) return next(err);
        res.json({})
      });
    });
  });

  router.post('/:id/:status', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must be logged in to update this notification.');
    }
    const params = { '_id': req.params.id };
    Notification.findOne(params, (err, notification) => {
      notification.status = req.params.status;
      notification.save(err => {
        if (err) return next(err);
        res.json(notification);
      });
    });
  });

  return router;
}
