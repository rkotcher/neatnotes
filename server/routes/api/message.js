import assert from 'assert';
import express from 'express';

import {
  NoteUserTagAssociation,
  Message,
  User,
} from '../../models';

const router = express.Router();

function getRoomId(noteId) {
  return `nn_message_${ noteId }`;
}

function getTagsFromText(text) {
  return text.match(/\B\#\w\w+\b/g);
}

export default function(app, io) {
  io.on('connection', socket => {
    socket.on('register_message', data => {
      assert.notEqual(data.noteId, undefined);
      socket.join(getRoomId(data.noteId));
    });

    socket.on('unregister_message', data => {
      assert.notEqual(data.noteId, undefined);
      socket.leave(getRoomId(data.noteId));
    });
  });

  router.post('/', (req, res, next) => {
    assert.notEqual(req.body.noteId, undefined);
    assert.notEqual(req.body.message.text, undefined);
    assert.notEqual(req.session.userId, undefined);

    const params = {
      'text': req.body.message.text,
      'noteId': req.body.noteId,
      'userId': req.session.userId,
    };

    Message.create(params).then(message => {
      User.findOne({ '_id': message.userId }, (err, user) => {
        if (err) {
          return next(err);
        }
        message = message.toObject();
        message.user = user;

        io.to(getRoomId(req.body.noteId)).emit('message', message);
        res.json({});
      });
    });

    const tags = params.text.match(/\B\#\w\w+\b/g);
    tags && tags.forEach(tag => {
      const tagParams = {
        'noteId': req.body.noteId,
        'userId': req.session.userId,
        'tagText': tag.substr(1),
      };

      NoteUserTagAssociation.findOne(tagParams, (err, association) => {
        !association && NoteUserTagAssociation.create(tagParams, (err, association) => {
          if (err) return console.error(`Couldn\'t create tag ${ tag.substr(1) }`);
        });
      });
    });
  });

  return router;
}
