import _ from 'underscore';
import bufferStream from 'simple-bufferstream';
import express from 'express';
import mongoose from 'mongoose';
import request from 'request';

import {
  Note,
  NoteUserAssociation,
  NoteUserTagAssociation,
  User
} from '../../models';

const router = express.Router();

function fillNote(note, userId) {
  note = note.toObject();

  return new Promise((resolve, reject) => {
    const userIds = _.pluck(note.messages, 'userId').map(id => {
      return mongoose.Types.ObjectId(id);
    });

    User.findOne({ '_id': note.authorId }, (err, author) => {
      note.author = author;
      delete note.authorId;

      User.find({ '_id': { '$in': userIds } }, (err, users) => {
        if (err) return reject(err);

        note.messages.forEach(message => {
          message['user'] = _.filter(users, user => {
            return user._id.equals(
              mongoose.Types.ObjectId(message.userId)
            );
          })[0];
          delete message.userId;
        });

        // an "abridged" version of this note. No tags to be included here.
        if (!userId) {
          return resolve(note);
        }

        const tagParams = {
          'noteId': note._id,
          'userId': userId
        };
        NoteUserTagAssociation.find(tagParams, (err, tags) => {
          if (err) return next(err);
          note.tags = tags;
          resolve(note);
        });
      });
    });
  });
}

export { fillNote };

export default function(app) {
  function base64ToGridId(base64) {
    return new Promise((resolve, reject) => {
      const stringData = base64.replace(/^data:image\/\w+;base64,/, '');
      const bufferData = new Buffer(stringData, 'base64');
      const gridfs = app.get('gridfs');

      const writeStream = gridfs.createWriteStream({
        'filename': 'screenshot.png',
      });
      writeStream.on('error', reject);
      writeStream.on('close', resolve);

      bufferStream(bufferData).pipe(writeStream);
    });
  }

  router.post('/', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must sign in to create a note.');
    }

    res.json({ '_id': new mongoose.mongo.ObjectId() });
  });

  router.post('/:id', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must sign in to update a note.');
    }

    base64ToGridId(req.body.screenshot.base64).then(response => {
      const data = {
        '_id': req.params.id,
        'authorId': req.session.userId,
        'href': req.body.href,
        'screenshot': {
          'fileId': response._id,
          'viewportWidth': req.body.screenshot.viewportWidth,
          'viewportHeight': req.body.screenshot.viewportHeight,
        },
        'clickLocation': {
          'mouseX': req.body.clickLocation.mouseX,
          'mouseY': req.body.clickLocation.mouseY,
        },
      };
      Note.create(data, (err, note) => {
        if (err) return next(err);
        res.json(note);
      });
    }).catch(next);
  });

  router.post('/:id/add_tag', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must sign in to tag a note.');
    }

    const params = {
      'userId': req.session.userId,
      'noteId': req.params.id,
      'tagText': req.body.tagText,
    };
    NoteUserTagAssociation.findOne(params, (err, association) => {
      if (!association) {
        NoteUserTagAssociation.create(params, (err, association) => {
          if (err) return next(err);
          res.json(association);
        });
      } else {
        res.json(association);
      }
    });
  });

  router.post('/:id/add_user', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must sign in to join a note.');
    }
    const params = {
      'userId': req.session.userId,
      'noteId': req.params.id,
    };
    NoteUserAssociation.findOne(params, (err, association) => {
      if (!association) {
        NoteUserAssociation.create(params, (err, association) => {
          if (err) return next(err);
          res.json(association);
        });
      } else {
        res.json(association);
      }
    });
  });

  router.get('/:id', (req, res, next) => {
    const query = {
      '_id': req.params.id,
    };
    Note.findOne(query, (err, note) => {
      if (err) return res.status(500).send({ 'message': `Error calling findOne with ${ query }`});
      if (!note) return res.status(404).send({ 'message': 'The requested resource could not be found' });
      fillNote(note).then(newNote => {
        res.json(newNote);
      }).catch(next);
    });
  });

  router.get('/', (req, res, next) => {
    if (!req.session.userId) {
      return next('You must be logged in to get your lists');
    }
    const query = {
      'userId': req.session.userId,
    };
    NoteUserAssociation.find(query, (err, associations) => {
      if (err) return next(err);

      const query = {
        '_id': { '$in': associations.map(association => association.noteId) },
      };

      Note.find(query, (err, notes) => {
        if (err) return next(err);
        Promise
          .all(notes.map(note => fillNote(note, req.session.userId) ))
          .then(filledNotes => res.json(filledNotes), next);
      });
    });
  });

  return router;
}
