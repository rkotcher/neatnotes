import express from 'express';
import request from 'request';

import { User } from '../../models';

const router = express.Router();

function createSession(req, res, next) {
  return (err, user) => {
    if (err) return next();
    req.session.userId = user._id;
    req.session.cookie.expires = true
    req.session.cookie.maxAge = 10000000000; // ~115 days
    return res.json(user);
  };
}

export default function(app) {
  router.get('/session', (req, res, next) => {
    if (!req.session.userId) {
      return res.json({ '_id': null });
    }
    const query = {
      '_id': req.session.userId,
    };
    User.findOne(query, (err, user) => {
      if (err) return res.json({ '_id': null });
      res.json(user);
    });
  });

  router.post('/logout', (req, res, next) => {
    req.session.destroy(err => {
      if (err) return next(err);
      res.json({});
    });
  });

  router.post('/facebook', (req, res, next) => {
    const token = req.body.accessToken;
    const url = `https://graph.facebook.com/me?access_token=${ token }`;

    request(url, (err, _res, body) => {
      if (err) return next(err);

      const data = JSON.parse(body);

      User.findOne({ 'facebookId': data.id }).exec((err, user) => {
        if (err) return next(err);
        if (user) return createSession(req, res)(null, user);

        User.create({
          'facebookId': data.id,
          'displayName': req.body.displayName,
          'email': req.body.email,
        }, createSession(req, res, next));
      });
    });
  });

  return router;
}
