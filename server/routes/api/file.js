import express from 'express';
import request from 'request';

const router = express.Router();

export default function(app) {
  router.get('/:id', (req, res, next) => {
    const gridfs = app.get('gridfs');

    const query = {
      '_id': req.params.id,
    };
    const readstream = gridfs.createReadStream(query);
    readstream.on('error', next);
    readstream.pipe(res);
  });

  return router;
}
